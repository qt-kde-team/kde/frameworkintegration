Source: frameworkintegration
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Patrick Franz <deltaone@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               dh-sequence-kf5,
               dh-sequence-pkgkde-symbolshelper,
               extra-cmake-modules (>= 5.115.0~),
               libappstreamqt5-dev (>= 1.0.0~),
               libkf5config-dev (>= 5.115.0~),
               libkf5configwidgets-dev (>= 5.115.0~),
               libkf5i18n-dev (>= 5.115.0~),
               libkf5iconthemes-dev (>= 5.115.0~),
               libkf5newstuff-dev (>= 5.115.0~),
               libkf5notifications-dev (>= 5.115.0~),
               libkf5package-dev (>= 5.115.0~),
               libkf5widgetsaddons-dev (>= 5.115.0~),
               libpackagekitqt5-dev,
               pkgconf,
               qtbase5-dev (>= 5.15.2~),
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/frameworks/frameworkintegration
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/frameworkintegration
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/frameworkintegration.git
Rules-Requires-Root: no

Package: frameworkintegration
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: KF5 cross-framework integration plugins
 Framework Integration is a set of plugins responsible
 for better integration of Qt applications when running
 on a KDE Plasma workspace.

Package: libkf5style-dev
Section: libdevel
Architecture: any
Depends: libkf5configwidgets-dev (>= 5.115.0~),
         libkf5iconthemes-dev (>= 5.115.0~),
         libkf5style5 (= ${binary:Version}),
         ${misc:Depends},
Description: KF5 cross-framework integration plugins - KStyle
 Framework Integration is a set of plugins responsible
 for better integration of Qt applications when running
 on a KDE Plasma workspace.
 .
 libkf5style5 provides integration with KDE Plasma Workspace settings
 for Qt styles.
 .
 Derive your Qt style from KStyle to automatically inherit
 various settings from the KDE Plasma Workspace, providing a
 consistent user experience. For example, this will ensure a
 consistent single-click or double-click activation setting,
 and the use of standard themed icons.
 .
 This package provides the development files.

Package: libkf5style5
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: KF5 cross-framework integration plugins - KStyle
 Framework Integration is a set of plugins responsible
 for better integration of Qt applications when running
 on a KDE Plasma workspace.
 .
 libkf5style5 provides integration with KDE Plasma Workspace settings
 for Qt styles.
 .
 Derive your Qt style from KStyle to automatically inherit
 various settings from the KDE Plasma Workspace, providing a
 consistent user experience. For example, this will ensure a
 consistent single-click or double-click activation setting,
 and the use of standard themed icons.
